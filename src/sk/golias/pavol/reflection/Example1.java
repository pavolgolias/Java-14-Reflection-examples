package sk.golias.pavol.reflection;

import java.lang.reflect.Method;
import java.util.Arrays;

// based on:
// http://www.mkyong.com/java/how-to-use-reflection-to-call-java-method-at-runtime/
// http://tutorials.jenkov.com/java-reflection/index.html

public class Example1 {
    public static void main(String[] args) {
        Class noParams[] = {};

        Class[] paramString = new Class[1];
        paramString[0] = String.class;

        Class[] paramInt = new Class[1];
        paramInt[0] = Integer.TYPE;

        try {
            Class cls = Class.forName("sk.golias.pavol.reflection.AppTest");
            Object obj = cls.newInstance();

            // call the printIt method
            Method method = cls.getDeclaredMethod("printIt", noParams);
            method.invoke(obj);

            // call the printString method, pass a String param
            method = cls.getDeclaredMethod("printItString", paramString);
            method.invoke(obj, "TEST 1");

            // call the printInt method, pass a int param
            method = cls.getDeclaredMethod("printItInt", int.class);
            method.invoke(obj, 123);

            // call the setCounter method, pass a int param
            method = cls.getDeclaredMethod("setCounter", paramInt);
            method.invoke(obj, 446);

            // call the printCounter method
            method = cls.getDeclaredMethod("printCounter");
            method.invoke(obj);

            for (Method tmp : cls.getDeclaredMethods()) {
                System.out.println("method = " + tmp.getName() + " / " + Arrays.toString(tmp.getParameterTypes()));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
